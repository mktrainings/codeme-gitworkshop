# Codeme-GitWorkshop

Projekt i repozytorium na warsztaty Git, 20-04-2020

Projekt ten będzie wykorzystywany na potrzeby warszatów z Git. Aby uzyskać dostęp, proszę, wyślij tzw. **Access Request** (odpowiedni link znajduje się zaraz obok ID projektu). 
Umożliwi to operacje na repozytorium warsztatów - tworzenie własnych gałęzi i wystawianie tzw. merge requestów.

# Materiały i opis zadań z workshopu

https://gitlab.com/mktrainings/codeme-gitworkshop/-/wikis/home

# Organizator
Organizatorem warsztatów jest fundacja [CODE:ME](https://codeme.pl/)